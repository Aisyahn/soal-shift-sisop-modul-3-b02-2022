# Soal-shift-sisop-modul-2-B02-2022

## Daftar Isi
* [Soal1](#soal-1)
* [Soal2](#soal-2)
* [Soal3](#soal-3)
* [dokumentasi pengerjaan dan kendala](#dokumentasi-pengerjaan-dan-kendala)

<hr>

# __Soal 1__
1. Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread
	
	a. 
	``` c
	...

	char cur_dir[256];
	char *names[] = {"quote","music"};
	char *links[] = {
			"https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
				"https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
	};
	void download(){
		pid_t new_id;
		for(int i = 0; i < 2; i++){
			new_id = fork();
			if(new_id < 0) exit(EXIT_FAILURE);
			if(new_id == 0){
				//make var name = names + .zip
				char name[strlen(names[i]) + strlen(".zip") + 1];
				strcpy(name, names[i]);
				strcat(name, ".zip");
				printf("Successfully downloaded %s directory\n",name);
				char *argv[] = {"wget", "-q", "--no-check-certificate",links[i],"-O",name,NULL};
				execv("/usr/bin/wget",argv);
			}
			waitpid(new_id,NULL,0);
		}

	}

	...

	```
	- mendownload dua file zip yang berbeda, yaitu `quote.zip` dan `music.zip` yang sudah ada pada link yang tertera

	``` c
	...

	void *unzip(void *index){
		int i = *((int*) index);
		pid_t new_id;
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			DIR *temp = opendir(names[i]);
			if(temp){
				int temp_id = fork();
				if(temp_id < 0) exit(EXIT_FAILURE);
				if(temp_id == 0){
					char *argv[] = {"rm","-r",names[i],NULL};
					execv("/usr/bin/rm",argv);
				}
				waitpid(temp_id,NULL,0);
				printf("Successfully deleted %s directory\n",names[i]);
			}
			char *argv[] = {"mkdir","-p",names[i],NULL};
			execv("/usr/bin/mkdir",argv);
		}
		waitpid(new_id,NULL,0);
		printf("Successfully created %s directory\n",names[i]);
		
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char name[strlen(names[i]) + strlen(".zip") + 1];
			strcpy(name, names[i]);
			strcat(name, ".zip");
			printf("Successfully unzipped %s to %s directory\n",name,names[i]);
			char *argv[] = {"unzip","-qq",name,"-d",names[i],NULL};
			execv("/usr/bin/unzip",argv);
		}
		waitpid(new_id,NULL,0);
		
	}

	...

	```
	- meng-unzip file zip, dengan mengecek terlebih dahulu apakah directory sudah ada atau belum, jika sudah ada maka akan dihapus
	- jika belum, maka akan dibuat directory baru dengan nama `quote` dan `music`

	b. 

	``` c
	...

	void *decode(void *index){
		int i = *((int*) index);
		char name[strlen(names[i]) + strlen(".txt") + 1];
		strcpy(name, names[i]); strcat(name, ".txt");
		FILE *output = fopen(name, "w");
		int link[2];
		
		DIR *dir_scan;
		struct dirent *entry;
		if((dir_scan = opendir(names[i])) == NULL) exit(EXIT_FAILURE);
		while((entry = readdir(dir_scan)) != NULL){
			if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
			char *foo = (char*) malloc(sizeof(char) * 4096);
			if(pipe(link) == -1) exit(EXIT_FAILURE);
			pid_t new_id = fork();
			if(new_id < 0) exit(EXIT_FAILURE);
			if(new_id == 0){
				char temp[strlen(entry->d_name) + strlen(names[i]) + 2];
				strcpy(temp, names[i]);
				strcat(temp,"/");
				strcat(temp, entry->d_name);
				
				dup2(link[1], STDOUT_FILENO);
				close(link[0]);close(link[1]);
				
				char *argv[] = {"base64","-d",temp,NULL};
				execv("/usr/bin/base64",argv);
			}
			waitpid(new_id,NULL,0);
			close(link[1]);
			read(link[0],foo,4096);
			fprintf(output, "%s\n",foo);
		}
		fclose(output);		
	}

	...
	```
	- men-decode semua file .txt yang ada dengan base 64 yang akan menghasilkan dua file .txt

	c. 

	```c
	...

	void hasil(){
		//mkdir hasil
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			DIR *temp = opendir("hasil");
			if(temp){
				int temp_id = fork();
				if(temp_id < 0) exit(EXIT_FAILURE);
				if(temp_id == 0){
					char *argv[] = {"rm","-r","hasil",NULL};
					execv("/usr/bin/rm",argv);
				}
				waitpid(temp_id,NULL,0);
				printf("Successfully deleted hasil directory\n");
			}
			char *argv[] = {"mkdir","-p","hasil",NULL};
			execv("/usr/bin/mkdir",argv);
		}
		waitpid(new_id, NULL, 0);
		
		//moving hasil to hasil directory
		for(int i = 0; i < 2; i++){
			char *name = (char*) malloc(strlen(names[i]) + strlen(".txt") + 1);
			strcpy(name, names[i]); strcat(name, ".txt");
			char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
			strcpy(dirtemp, cur_dir);
			strcat(dirtemp,"/");strcat(dirtemp,"hasil");
			new_id = fork();
			if(new_id < 0) exit(EXIT_FAILURE);
			if(new_id == 0){
				char *argv[] = {"mv",name,dirtemp,NULL};
				execv("/usr/bin/mv",argv);
			}
			waitpid(new_id,NULL,0);
		}
	}

	...

	```
	- membuat directory baru `hasil`, jika sudah ada maka akan dihapus terlebih dahulu
	- lalu memindahkan hasil dari dua file .txt sebelumnya ke diretory `hasil`
	

	d. 

	``` c
	...
	void zip_hasil(char *password){
		//zip hasil
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"zip","-q","-P",password,"-r","hasil.zip","hasil",NULL};
			execv("/usr/bin/zip",argv);
		}
		waitpid(new_id,NULL,0);
		if((new_id = fork()) == -1) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"rm", "-r", "hasil", NULL};
			execv("/usr/bin/rm",argv);
		}
		waitpid(new_id,NULL,0);
		puts("Successfully zipped hasil to hasil.zip");
	}

	...

	```
	- meng-zip folder `hasil` disertai password
	
	e. 

	``` c
	...
	void *unzip_hasil(void *_password){
		char *password = (char*) _password;
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"unzip","-q","-P",password,"hasil.zip",NULL};
			execv("/usr/bin/unzip",argv);
		}
		waitpid(new_id,NULL,0);
		unzipped_hasil = 1;
	}

	void *no_txt(){
		FILE *no = fopen("no.txt","w");
		fprintf(no,"No\n");
		fclose(no);
		DIR *hasil;
		while(!unzipped_hasil){
		}
		char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
		strcpy(dirtemp, cur_dir);
		strcat(dirtemp,"/");strcat(dirtemp,"hasil");
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"mv","no.txt",dirtemp,NULL};
			execv("/usr/bin/mv",argv);
		}
		waitpid(new_id,NULL,0);
	}

	...
	```
	- meng-unzip file `hasil.zip` 
	- membuat file `no.txt` 
	
	```c
	...

	zip_hasil(password);

	...
	```
	- memanggil fungsi `zip_hasil` untuk meng-zip file `hasil` dan file `no.txt` menjadi `hasil.zip`
		
<hr>

# __Soal 2__

2. Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

	a.	Pada saat client terhubung ke server, terdapat dua 	pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

	- Username unique (tidak boleh ada user yang memiliki username yang sama)
	- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.
	
	Format users.txt:
	
	__users.txt__
	> username:password\
	username2:password2

	``` c
		int register_user(int socket, char *arg){
			char *username, *password, *response;
			username = strtok(arg, " ,.-");
			password = strtok(NULL, "");

			int check_user, check_pass;
			check_user = valid_user(username, strlen(username));
			check_pass = valid_pass(password, strlen(password));

			if(check_user){
				response = "username has already been taken\n";
				write(socket, response , strlen(response));
				return EXIT_FAILURE;
			} 

			if(check_pass){
				response = "password must contain atleast 6 character, a number, a lowercase, and an uppercase\n";
				write(socket, response , strlen(response));
				return EXIT_FAILURE;
			} 

			write_user_info(username, password);
			response = "user registered succesfully\n";
			write(socket, response , strlen(response));
			return EXIT_SUCCESS;
		}

		int login_user(int socket, char *arg){
			char *username, *password, *response;
			username = strtok(arg, " ,.-");
			password = strtok(NULL, "");

			if(login_check(username, password)) {
				response = "logged in successfully";
				write(socket, response , strlen(response));
				return EXIT_SUCCESS;
			} else {
				response = "wrong credentials";
				write(socket, response , strlen(response));
				return EXIT_FAILURE;
			}
		}
	```
	Register User : mendaftarkan user ke server melalui input user
	- ambil username dan password dari input user
	- cek apakah input user valid atau tidak
	- jika valid, simpan data user ke user.txt

	Login User
	- ambil username dan password dari input user
	- cek apakah input user terdapat di user.txt atau tidak
	- jika valid, sambut user ke server

	b. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama __problems.tsv__ yang terdiri dari __judul problem dan author problem (berupa username dari author), yang dipisah dengan \t.__ File otomatis dibuat saat server dijalankan.	

	c. __Client yang telah login__, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
	- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
	- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
	- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
	- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

	Contoh :

	Client-side
	> add

	Server-side
	> Judul problem:\
	Filepath description.txt:\
	Filepath input.txt:\
	Filepath output.txt:

	Client-side
	>  < judul-problem-1>\  
	<Client/description.txt-1>\
	<Client/input.txt-1>\
	<Client/output.txt-1>

	Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file __problems.tsv__.

	d. __Client yang telah login__, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

	> < judul-problem-1> by < author1> \
	< judul-problem-2> by < author2>

	e. __Client yang telah login__, dapat memasukkan command ‘download < judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu < judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama < judul-problem> di client.

	f.	__Client yang telah login__, dapat memasukkan command ‘submit < judul-problem> < path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

	g.	Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

<hr>

# __Soal 3__

3. Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

	![soal3_1](/uploads/3e81768409ba53ff0933bcacaf3ed36a/soal3_1.png)  


	a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
    

    b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

    c. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

    d. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

	
	e. Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server

	> send hartakarun.zip

	 
<hr>

## Dokumentasi pengerjaan dan kendala

### Soal 1
Dokum :

Berikut adalah beberapa kendala yang saya jumpai selama mengerjakan soal1 :
	- 

### Soal 2
Dokum :

![image](/uploads/c54d3d8b5e85d3995cd6293afa839ad9/image.png)
![image](/uploads/69659eb189ba84048ca981173b301560/image.png)
![image](/uploads/cf82216468489e4449a9bfb02d81708c/image.png)

Berikut adalah beberapa kendala yang saya jumpai selama mengerjakan soal2 :

	- banyak tugas lain hehe
	- belum terlalu paham client server & socket programming
	- kurang paham cara ngepass argumen di fungsi thread
	- return value fungsi cek nya ternyata kebalik
	- terlalu lama berkutik di struktur program

### Soal 3
Dokum :

Berikut adalah beberapa kendala yang saya jumpai selama mengerjakan soal3 :
	- 
