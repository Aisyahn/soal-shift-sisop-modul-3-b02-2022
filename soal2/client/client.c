#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#define PORT 8080
#define IP_PROTOCOL 0
#define REQ_LEN 100
#define RESP_LEN 2000

int isEqual(char *str1, char *str2);
void *sendRequest();
void *readResponse();
  
int main(int argc, char const *argv[]) {
	int clientSocket;
	struct sockaddr_in serverAddr;
	char *request, serverResp[2000];

	// create client socket for IPv4, TCP, and IP Protocol
	if ((clientSocket = socket(AF_INET, SOCK_STREAM, IP_PROTOCOL)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	// set server address stuff
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;

	// connect client socket to server address
	if (connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) {
		perror("connection attempt failed");
		exit(EXIT_FAILURE);
	}

	puts("Client is running");

	pthread_t writeThread, readThread;
	int *clientSocketPtr, errorStatus;

	clientSocketPtr = malloc(1);
	*clientSocketPtr = clientSocket;

	errorStatus = pthread_create(&writeThread, NULL, sendRequest, (void *) clientSocketPtr);
	if(errorStatus) {
		fprintf(stderr, "writeThread creation failed with code : %d\n", errorStatus);
		exit(EXIT_FAILURE);
	}

	errorStatus = pthread_create(&readThread, NULL, readResponse, (void *) clientSocketPtr);
	if(errorStatus) {
		fprintf(stderr, "readThread creation failed with code : %d\n", errorStatus);
		exit(EXIT_FAILURE);
	}

	pthread_join(writeThread, NULL);
	return 0;
}

int isEqual(char *str1, char *str2){
	if(strcmp(str1, str2) == 0) return 1;
	else return 0;
}

void *sendRequest(void *socketPtr) {
	// get the socket descriptor
	int socket = *(int*) socketPtr;
	char request[REQ_LEN];

	do{
		fgets(request, REQ_LEN, stdin);
		// remove \r and \n
		request[strcspn(request, "\r\n")] = 0; 
		if(write(socket, request, strlen(request)) >= 0) {
			printf("request content : %s\n", request);
		} else {
			perror("failed to send request");
			exit(EXIT_FAILURE);
		}
	} while(!isEqual(request, "abort"));
}

void *readResponse(void *socketPtr) {
	// get the socket descriptor
	int socket = *(int*) socketPtr;
	char serverResp[RESP_LEN];
	
	// receive a response from server
	while((read(socket, serverResp, RESP_LEN) >= 0)) {
		printf("response content : %s\n", serverResp);
		// reset buffer
		memset(serverResp, 0, sizeof(serverResp));
	}
}