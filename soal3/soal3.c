#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<pthread.h>

FILE *output;
char cur_dir[512], hasil_dir[1024];

void lower(char *str){
	int i = -1;
	while(str[++i] != '\0'){
		if('A' <= str[i] && str[i] <= 'Z') str[i] += 32;
	}
}

char *extension(char *filename){
	char *output;
	//return strrchr(filename, '.') + 1;
	if(filename[0] == '.') return "hidden";
	int i = 1;
	while(filename[i] != '\0' && filename[i] != '.') i++;
	if(!filename[i]) return "unknown";
	sprintf(output, "%s", filename + i + 1);
	lower(output);
	return output;
}

void remove_directory(char *dir_name){
	DIR *dir_scan = opendir(dir_name);
	if(dir_scan == NULL) return;
	struct dirent *entry;
	chdir(dir_name);
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
		if(entry->d_type == DT_DIR){
			if(rmdir(entry->d_name) == 0) continue;
			remove_directory(entry->d_name);
		}
		else remove(entry->d_name);
	}
	chdir("..");
	rmdir(dir_name);
}


struct folder{
	DIR *dir;
	char path[1024];
};

struct file{
	char *path,*name,*ext;
};

int active_folder = 0;
void *scan_dir(void* _cur){
	struct folder *cur = (struct folder*) _cur;
	struct dirent *entry;
	DIR *temp;
	while((entry = readdir(cur->dir)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
		char *ext;
		if(entry->d_type == DT_DIR){
			struct folder *sub_folder = (struct folder*)
				malloc(sizeof(struct folder));
			strcpy(sub_folder->path, cur->path);
			strcat(sub_folder->path, "/");
			strcat(sub_folder->path, entry->d_name);
			
			sub_folder->dir = opendir(sub_folder->path);
			if(sub_folder->dir == NULL) continue;
			active_folder++;
			pthread_t thread;
			thread = pthread_create(&thread, NULL, scan_dir, (void*) sub_folder);
			ext = "directory";
			continue;
		}
		else ext = extension(entry->d_name);
		char from[1000],dest[1000];
		strcpy(from, cur->path);
		strcat(from, "/");
		strcat(from,entry->d_name);
		strcpy(dest, hasil_dir);
		strcat(dest, "/");
		strcat(dest, ext);
		temp = opendir(dest);
		if(temp == NULL) mkdir(dest, S_IRWXU);
		strcat(dest, "/");
		strcat(dest, entry->d_name);
		rename(from, dest);
		fprintf(output, "%s\n%s\n",from,dest);
		 
		}
	active_folder--;
	//closedir(cur->dir);
	//free(_cur);
	pthread_exit(NULL);
}

int main(){
	getcwd(cur_dir, sizeof(cur_dir));
	output = fopen("log.txt","w");
	
	remove_directory("hartakarun");
	//unzip file
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"unzip","-q","hartakarun.zip",NULL};
		execv("/usr/bin/unzip", argv);
	}
	waitpid(new_id, NULL, 0);
	//---
	
	char *root_folder = "hartakarun";
	
	//make directory hasil kategorisasi
	remove_directory("hasil");
	mkdir("hasil", S_IRWXU);
	//path menuju directory hasil
	sprintf(hasil_dir, "%s/hasil",cur_dir);
	
	struct folder *root = (struct folder*) malloc(sizeof(struct folder));
	sprintf(root->path, "%s/%s",cur_dir, root_folder);
	if((root->dir = opendir(root->path)) == NULL) exit(EXIT_FAILURE);
	
	pthread_t thread;
	thread = pthread_create(&thread, NULL, scan_dir, (void*) root);
	active_folder++;
	
	//menunggu seluruh thread selesai
	while(active_folder > 0){
	}
	fclose(output);
	remove_directory(root_folder);
	rename("hasil",root_folder);
	
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"zip","-q", "-r", "hartakarunn.zip", "hartakarun" ,NULL};
		execv("/usr/bin/zip", argv);
	}
	waitpid(new_id, NULL, 0);
	
	return 0;
}
